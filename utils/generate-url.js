module.exports = function generateUrl(id) {
  const BASE_URL = process.env.BASE_URL;
  return `${BASE_URL}/l/${id}`
}