const Router = require('express')
const shortLinkController = require('../controllers/shortlink.controller')
const router = new Router()

router.get('/', shortLinkController.hello)
router.get('/:id', shortLinkController.redirectUrl)
router.post('/create-short-link', shortLinkController.createShortLink)

module.exports = router