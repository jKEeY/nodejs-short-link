const { URL } = require('url');
const shortlinkService = require('../services/shortlink.service')
const generateUrl = require('../utils/generate-url')

class ShortLinkController {
  async redirectUrl(req, res) {
    const { id } = req.params;
    const { longUrl } = await shortlinkService.getLinkId(id)

    if (!longUrl) {
      res.status(404).json({})
    } else {
      res.status(301).redirect(longUrl)
    }

    return res
  }

  // pod health check
  async hello(req, res) {
    return res.status(200).send('Hello World!')
  }

  async createShortLink(req, res) {
    const { url } = req.body;
    const id = await shortlinkService.getLink(new URL(url).href)
    const resultUrl = generateUrl(id)

    return res.json({ shortLink: resultUrl })
  }
}

module.exports = new ShortLinkController()