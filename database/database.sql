create table shortlinks (
	id serial primary key,
	link_id text,
	link text
);