const request = require("supertest");
const { app } = require('../app')

const VALID_URL = 'https://google.com'
const NO_VALID_ID = 'NO_VALID_ID'

describe('shortlink controller', () => {
  let server;
  beforeEach(() => {
    server = request(app)
  })

  it('POST /create-short-link return 200 status', async () => {
    const result = await server
      .post('/l/create-short-link')
      .send({ url: VALID_URL })

    expect(result.statusCode).toBe(200)
    expect(typeof result.body.shortLink).toBe('string')
  })
  it('GET /:id returns 301 status if the id is valid', async () => {
    const result = await server
      .post('/l/create-short-link')
      .send({ url: VALID_URL })

    expect(result.statusCode).toBe(200)
    expect(typeof result.body.shortLink).toBe('string')
    const SHORT_LINK = result.body.shortLink.split('/');
    const SHORT_LINK_ID = SHORT_LINK[SHORT_LINK.length - 1]

    const result2 = await server
      .get(`/l/${SHORT_LINK_ID}`)

    expect(result2.statusCode).toBe(302)
  })
  it('GET /:id return 404 if id is not valid', async () => {
    const result = await server
      .get(`/l/${NO_VALID_ID}`)

    expect(result.statusCode).toBe(404)
  }, 10000)
  it('If you create a second short link with the same url, the id of the first one will be returned', async () => {
    const result = await server
      .post('/l/create-short-link')
      .send({ url: VALID_URL })

    expect(result.statusCode).toBe(200)
    expect(typeof result.body.shortLink).toBe('string')

    const result2 = await server
      .post('/l/create-short-link')
      .send({ url: VALID_URL })

    expect(result2.statusCode).toBe(200)
    expect(typeof result2.body.shortLink).toBe('string')

    const SHORT_LINK_1 = result.body.shortLink.split('/')
    const SHORT_LINK_2 = result2.body.shortLink.split('/')
    const ID_1 = SHORT_LINK_1[SHORT_LINK_1.length - 1]
    const ID_2 = SHORT_LINK_2[SHORT_LINK_2.length - 1]

    expect(ID_1).toEqual(ID_2)
  })
})