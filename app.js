const express = require('express');
const bodyParser = require('body-parser')
const cors = require('cors')
const shortLinkRoutes = require('./routes/shortlink.router')

const app = new express()

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json({ limit:10000000 }))
app.use(cors())

app.use('/l', shortLinkRoutes)

module.exports = { app }