# build stage
FROM node:12.20.1 as build-stage

WORKDIR /app
COPY package*.json ./

RUN npm install

COPY . .

EXPOSE 8000

CMD [ "node", "index.js" ]
