const shortid = require('shortid');
const { db } = require('../database/database')

class ShortLink {
  async getLinkId(id) {
    const result = await db.query(`select link from shortlinks where shortlinks.link_id = $1`, [id])
    if (!result.rows.length) return { longUrl: null }
    
    return { longUrl: result.rows[0].link }
  }
  async getLink(url) {
    const result = await db.query(`select link_id from shortlinks where shortlinks.link = $1`, [url])
    if (result.rows.length) {
      return result.rows[0].link_id
    }

    const linkId = shortid.generate()
    await db.query(`insert into shortlinks(link_id, link) values ($1, $2)`, [linkId, url])

    return linkId
  }
}

module.exports = new ShortLink()
